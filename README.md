# cp-node-logger - v1.0.0

Nodejs logger for clickpesa projects with console, file and elasticsearch support

## Install

Add this package repository to package.json and run npm install

```js
"cp-node-logger": "git+ssh://git@bitbucket.org/payclick/cp-nodejs-logger.git"
```

Then run

```bash
npm install
```

## Initialize on Non-NestJs projects

```js
import { createLogger } from 'winston';
const { createWinstonConfig } = require('cp-node-logger');
OR;
import { createWinstonConfig } from 'cp-node-logger';

const configObj = createWinstonConfig({
  environment: process.env.NODE_ENV,
  type: 'application',
  level: process.env.LOG_LEVEL || 'info',
  source: process.env.APP_NAME || '',
  elasticTransport: {
    enabled: process.env.ELASTIC_ENABLED == 'true',
    host: process.env.ELASTIC_HOST,
    username: process.env.ELASTIC_USERNAME,
    password: process.env.ELASTIC_PASSWORD,
  },
  slackTransport: {
    enabled: true,
    webhookUrl: 'YOUR WEBHOOK URL',
  },
});
const logger = createLogger(configObj);

export default logger;
```

## Initialize on NestJS projects

Create a logger class

```ts
import { LoggerOptions } from 'winston';
import { createWinstonConfig } from 'cp-node-logger';

export class LoggerConfig {
  private readonly options: LoggerOptions;
  constructor() {
    const source = process.env.APP_NAME || '';
    const level = process.env.LOG_LEVEL || 'info';
    this.options = createWinstonConfig({
      environment: process.env.NODE_ENV,
      type: 'application',
      level,
      source,
      elasticTransport: {
        enabled: process.env.ELASTIC_ENABLED == 'true',
        host: process.env.ELASTIC_HOST,
        username: process.env.ELASTIC_USERNAME,
        password: process.env.ELASTIC_PASSWORD,
      },
      slackTransport: {
        enabled: true,
        webhookUrl: 'YOUR WEBHOOK URL',
      },
    });
  }
  public getOptions(): LoggerOptions {
    return this.options;
  }
}
```

In your main.ts file, import LoggerConfig class and replace the default logger

```ts
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import { LoggerConfig } from './config/winston.config';

const logger: LoggerConfig = new LoggerConfig();
async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(logger.getOptions()),
  });
  await app.listen(3000);
}
bootstrap();
```

## Usage

This package uses winston logger under the hood, so the logger functions are the same, for example,

```js
logger.info('Clickpesa info');

logger.info('Clickpesa info', {
  serviceId: 'SERVER_OPERATION_SERVICE',
  appID: 2,
  txnID: 299929,
});

logger.debug('Clickpesa debug', {
  serviceId: 'SERVER_OPERATION_SERVICE',
  appID: 2,
  txnID: 299929,
});
logger.error('Clickpesa error', {
  serviceId: 'SERVER_OPERATION_SERVICE',
  appID: 2,
  txnID: 299929,
});
```

## Author

👤 **ClickPesa Ltd**

- Website: clickpesa.com
- Twitter: [@clickpesa](https://twitter.com/clickpesa)
- Github: [@ClickPesa](https://github.com/'ClickPesa)
- LinkedIn: [@clickpesa](https://linkedin.com/in/clickpesa)

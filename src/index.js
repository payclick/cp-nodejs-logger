let { format, transports } = require('winston');
let { ElasticsearchTransport } = require('winston-elasticsearch');
let SlackHook = require('winston-slack-webhook-transport');

module.exports.createWinstonConfig = function (config = {}) {
  const { environment, type, source, level, elasticTransport, slackTransport } =
    config;

  const logFormat = format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`,
  );

  const consoleTransport = new transports.Console({
    format: format.combine(
      format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      format.colorize(),
      logFormat,
      // format.json()
    ),
  });

  const fileTransport = new transports.File({
    filename: 'logs/combined.log',
    format: format.combine(format.json()),
  });

  let transportsArray = [fileTransport];

  //if not prod add console transport
  // process.env.NODE_ENV
  if (environment !== 'production') {
    transportsArray.push(consoleTransport);
  }

  // add elastic transport if enabled
  if (elasticTransport && elasticTransport.enabled === true) {
    const { host, username, password } = elasticTransport;

    const elasticTransportObj = new ElasticsearchTransport({
      level,
      clientOpts: {
        node: host,
        auth: {
          username,
          password,
        },
      },
      source,
    });

    transportsArray.push(elasticTransportObj);
  }

  // add slack transport if enabled
  if (
    slackTransport &&
    slackTransport.enabled === true &&
    slackTransport.webhookUrl
  ) {
    const { webhookUrl } = slackTransport;

    const slackTransportObj = new SlackHook({
      level: slackTransport.level || level,
      webhookUrl,
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.errors({ stack: true }),
        format.splat(),
        format.json(),
      ),
      formatter: (info) => {
        return {
          attachments: [
            {
              fallback: source,
              color: '#fddc01',
              author_name: info.operation,
              author_link: '',
              author_icon: '',
              title: info.message,
              fields: [
                {
                  title: 'Received at',
                  value: new Date().toLocaleString(),
                  short: false,
                },
              ],
              footer: 'from: ' + source,
              footer_icon:
                'http://clickpesa.com/wp-content/uploads/2019/01/logo_compact.png',
            },
          ],
        };
      },
    });
    transportsArray.push(slackTransportObj);
  }

  return {
    level,
    defaultMeta: {
      type,
      sourceApp: source,
      environment,
    },
    exitOnError: false,
    format: format.combine(
      format.timestamp(),
      // Format the metadata object
      format.metadata({
        fillExcept: [
          'message',
          'level',
          'timestamp',
          'label',
          'type',
          'sourceApp',
          'environment',
          'serviceId',
        ],
      }),
    ),
    transports: transportsArray,
  };
};
